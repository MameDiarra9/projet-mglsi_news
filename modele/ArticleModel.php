<?php
class ArticleModel {
    private $db;

    public function __construct() {
        $this->db = new PDO('mysql:host=localhost;dbname=mglsi_news', 'mglsi_user', 'passer');
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function getAllArticles() {
        $query = "SELECT * FROM Article ORDER BY dateCreation DESC";
        $result = $this->db->query($query);
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getArticlesByCategory($categorie_id) {
        $query = "SELECT * FROM Article WHERE categorie = :categorie_id ORDER BY dateCreation DESC";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(':categorie_id', $categorie_id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function createArticle($titre, $contenu, $categorie) {
        $query = "INSERT INTO Article (titre, contenu, categorie) VALUES (:titre, :contenu, :categorie)";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(':titre', $titre, PDO::PARAM_STR);
        $stmt->bindValue(':contenu', $contenu, PDO::PARAM_STR);
        $stmt->bindValue(':categorie', $categorie, PDO::PARAM_INT);
        return $stmt->execute();
    }

    public function updateArticle($id, $titre, $contenu, $categorie) {
        $query = "UPDATE Article SET titre = :titre, contenu = :contenu, categorie = :categorie WHERE id = :id";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->bindValue(':titre', $titre, PDO::PARAM_STR);
        $stmt->bindValue(':contenu', $contenu, PDO::PARAM_STR);
        $stmt->bindValue(':categorie', $categorie, PDO::PARAM_INT);
        return $stmt->execute();
    }

    public function deleteArticle($id) {
        $query = "DELETE FROM Article WHERE id = :id";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        return $stmt->execute();
    }

    public function getArticleById($id) {
        $query = "SELECT * FROM Article WHERE id = :id";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
}
?>
