<?php
class CategorieModel {
    private $db;

    public function __construct() {
        $this->db = new PDO('mysql:host=localhost;dbname=mglsi_news', 'mglsi_user', 'passer');
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function getAllCategories() {
        $query = "SELECT * FROM Categorie";
        $result = $this->db->query($query);
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getCategorieById($categorie_id) {
        $query = "SELECT * FROM Categorie WHERE id = :categorie_id";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(':categorie_id', $categorie_id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
}
?>
