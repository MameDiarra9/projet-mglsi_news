<?php 
    require_once '../modele/ArticleModel.php';
    require_once '../modele/CategorieModel.php';
    require_once '../controleur/UserController.php';
?>
<!DOCTYPE html>
<html>
<head>
    <title>ESP News</title>
        <!-- Google Fonts -->
        <link
        href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600&display=swap"
        rel="stylesheet"
        />
        <!-- bootstrap -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
    </title>
    <link rel="stylesheet" href="../CSS/style.css">
</head>
<body>


    <div class="heading-container">
        <h4>ESP NEWS</h4>
        
    </div>
    <div>
            <?php if ($user) : ?>
                <p>Bienvenue, <?php echo $user['nom']; ?>! <a href="index.php?action=deconnexion">Déconnexion</a></p>
            <?php endif; ?>
    </div>
    <div class="menu-bar">
        <ul>
        <?php if ($user) : ?>
                    <li> Bienvenue, <?php echo $user['nom']; ?>! <a href="index.php?action=deconnexion" >Déconnexion</a></li>
        <?php endif; ?>
        <li><a href="../controleur/index.php?action=accueil">Accueil</a></li>
        <?php foreach ($categories as $categorie) : ?>
            <li><a href="../controleur/index.php?action=articles_par_categorie&categorie=<?php echo $categorie['id']; ?>"><?php echo $categorie['libelle']; ?></a></li>
        <?php endforeach; ?>
        </ul>
    </div>

    <div class="content">
        <div class="row row-cols-1 row-cols-md-3 g-4">
            <?php foreach ($articles as $article): ?>
                <div class="col">
                    <div class="card h-100">
                        <div class="card-body">
                            <h2 class="card-title"><?= $article['titre']; ?></h2>
                            <p class="card-text"><?= $article['contenu']; ?></p> 
                        </div>
                        <div class="card-footer">
                            <small>Catégorie: <?php echo $categorieModel->getCategorieById($article['categorie'])['libelle']; ?></small>
                            <small class="text-body-secondary">Dernière modification : <?= $article['dateModification']; ?></small>
                            <?php if ($user) : ?>
                                <a href="../controleur/index.php?action=modifier_article&id=<?php echo $article['id']; ?>">Modifier</a>
                                <a href="../controleur/index.php?action=supprimer_article&id=<?php echo $article['id']; ?>">Supprimer</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <!-- Ajouter un article  -->
    <?php if ($user) : ?>
        <h2>Ajouter un article</h2>
        <form method="post" action="../controleur/index.php?action=creer_article">
            <label>Titre:</label>
            <input type="text" name="titre" required>
            <label>Contenu:</label>
            <textarea name="contenu" required></textarea>
            <label>Catégorie:</label>
            <select name="categorie">
                <?php foreach ($categories as $categorie) : ?>
                    <option value="<?php echo $categorie['id']; ?>"><?php echo $categorie['libelle']; ?></option>
                <?php endforeach; ?>
            </select>
            <input type="submit" value="Créer">
        </form>
    <?php endif; ?>

</body>
</html>

