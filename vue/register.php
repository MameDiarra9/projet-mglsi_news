<?php
session_start();
require_once '../controleur/UserController.php';

$userController = new UserController();

if (isset($_POST['nom'], $_POST['email'], $_POST['password'])) {
    $nom = $_POST['nom'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $userController->registerUser($nom, $email, $password);
    // Redirection vers la page de connexion après l'inscription réussie
    header('Location: login.php');
}
?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="../CSS/login.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <title>Connexion</title>
</head>
<body>
        <div class="Container ">
            <div class="card mt-5" style="background-color: #fff !important; border:solid 0px !important;">
                <div class="upper">
                    <div class="row">
                        <div class="col-8 heading">
                            <h4><b>Inscrivez - Vous</b></h4>
                        </div>
                    </div>
                    <?php if (isset($error_message)) : ?>
                        <p><?php echo $error_message; ?></p>
                    <?php endif; ?>
                    <form id="formL" method="post" action="register.php">

                        <div id="alert" class="mt-1" role="alert">
                        </div>
                        <div class="form-element">
                            <span id="input-header">Prénom et Nom</span>
                            <input type="text" id="order_id" name="nom" placeholder="Prénom et Nom" required> 
                        </div>
                        <div class="form-element">
                            <span id="input-header">E-mail</span>
                            <input type="email" id="order_id" name="email" placeholder="email@gmail.com" required> 
                        </div>
                        <div class="form-element ">
                            <span id="input-header">Mot de pase</span>
                            <div class="row p-0">
                                <div class="d-flex align-items-center">
                                    <input type="password" name="password" id="phone" placeholder="********" required>
                                    <i class="fas fa-eye-slash eye"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-element">
                            <div class="row invalid">
                                <div class="col-11">
                                    <h6 class="text-secondary mb-3 text-center">
                                        <p class="mb-0">j'ai deja un compte</p><a href="login.php" class="text-info">Se connecter </a>
                                </div>
                            </div>
                        </div>
                        <div class="lower">
                            <button class="btn btn-danger" type="submit" id="loader">S'inscrire</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</body>
</html>

<script>
        // declaration des variables
    let eye = document.querySelector('.eye')
    let password = document.getElementById('phone')

    /**
     * cette class contient l'ensemble des actions a effectuer sur cette page
     */
    class action {
        toglePassWord(tag) {
            let ClassName = tag.classList.contains('fa-eye-slash')
            if (ClassName == true) {
                tag.classList.replace("fa-eye-slash", "fa-eye")
                password.setAttribute('type', "text")
            } else {
                tag.classList.replace("fa-eye", "fa-eye-slash")
                password.setAttribute('type', "password")
            }
        }
    }

    // appel de la classe action
    let make = new action()
    eye.addEventListener('click', function (e) {
        make.toglePassWord(eye)

    })
</script>
