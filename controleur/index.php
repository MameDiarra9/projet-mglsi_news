<?php
session_start();
require_once '../modele/ArticleModel.php';
require_once '../modele/CategorieModel.php';
require_once 'UserController.php';

$action = isset($_GET['action']) ? $_GET['action'] : 'accueil';

$articleModel = new ArticleModel();
$categorieModel = new CategorieModel();
$userController = new UserController();

// Vérifier si l'utilisateur est connecté
$user = null;
if (isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
}

// Action de déconnexion
if ($action === 'deconnexion') {
    session_unset();
    session_destroy();
    // Redirection vers la page d'accueil après la déconnexion
    header('Location: index.php?action=accueil');
}

// Vérifier l'accès aux articles
if ($action === 'accueil' && !$user) {
    header('Location: ../vue/login.php');
    exit; // Arrêter l'exécution du reste du script
}

// Gestion des autres actions
switch ($action) {
    case 'accueil':
        $articles = $articleModel->getAllArticles();
        $categories = $categorieModel->getAllCategories();
        include '../vue/view.php';
        break;
    
    case 'articles_par_categorie':
        if (isset($_GET['categorie'])) {
            $categorie_id = $_GET['categorie'];
            $articles = $articleModel->getArticlesByCategory($categorie_id);
            $categories = $categorieModel->getAllCategories();
            include '../vue/view.php';
        } else {
            // Redirection vers la page d'accueil si aucune catégorie spécifiée.
            header('Location: index.php?action=accueil');
        }
        break;

    case 'creer_article':
        // Vérifier si l'utilisateur est connecté avant de lui permettre de créer un article
        if ($user) {
            if (isset($_POST['titre'], $_POST['contenu'], $_POST['categorie'])) {
                $titre = $_POST['titre'];
                $contenu = $_POST['contenu'];
                $categorie = $_POST['categorie'];
                $articleModel->createArticle($titre, $contenu, $categorie);
                // Redirection vers la page d'accueil après la création de l'article
                header('Location: index.php?action=accueil');
            } else {
                $categories = $categorieModel->getAllCategories();
                include '../vue/view.php';
            }
        } else {
            // Redirection vers la page de connexion si l'utilisateur n'est pas connecté
            header('Location: index.php');
        }
        break;

    case 'modifier_article':
        // Vérifier si l'utilisateur est connecté avant de lui permettre de modifier un article
        if ($user) {
            if (isset($_GET['id'])) {
                $article_id = $_GET['id'];
                $article = $articleModel->getArticleById($article_id);
                if ($article) {
                    if (isset($_POST['titre'], $_POST['contenu'], $_POST['categorie'])) {
                        $titre = $_POST['titre'];
                        $contenu = $_POST['contenu'];
                        $categorie = $_POST['categorie'];
                        $articleModel->updateArticle($article_id, $titre, $contenu, $categorie);
                        // Redirection vers la page d'accueil après la modification de l'article
                        header('Location: index.php?action=accueil');
                    } else {
                        $categories = $categorieModel->getAllCategories();
                        include '../vue/view.php';
                    }
                } else {
                    // Redirection vers la page d'accueil si l'article n'est pas trouvé
                    header('Location: index.php?action=accueil');
                }
            } else {
                // Redirection vers la page d'accueil si l'id de l'article n'est pas spécifié
                header('Location: index.php?action=accueil');
            }
        } else {
            // Redirection vers la page de connexion si l'utilisateur n'est pas connecté
            header('Location: index.php');
        }
        break;

    case 'supprimer_article':
        // Vérifier si l'utilisateur est connecté avant de lui permettre de supprimer un article
        if ($user) {
            if (isset($_GET['id'])) {
                $article_id = $_GET['id'];
                $articleModel->deleteArticle($article_id);
            }
            // Redirection vers la page d'accueil après la suppression de l'article
            header('Location: index.php?action=accueil');
        } else {
            // Redirection vers la page de connexion si l'utilisateur n'est pas connecté
            header('Location: index.php');
        }
        break;

    default:
        // Redirection vers la page d'accueil pour toute autre action non reconnue.
        header('Location: index.php?action=accueil');
}
?>
